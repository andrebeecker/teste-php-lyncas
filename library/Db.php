<?php

namespace TestePHP\Library;

class Db {

    protected $con;
    
    /** Cria uma conexão com o banco */
    function __construct(){
        $this->con = new \SQLite3('../base.sq3');
    }

    public function executeQuery( $sql, $fetch = false ){
        
        try {
            $result = $this->con->query($sql);

            return [
                'success' => true,
                'response' => $result,
                'message' => 'Sql executado'
            ];

        } catch (\Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
    }

    function fetch( $response ){

        $aData = [];

        $row = $response->fetchArray(SQLITE3_ASSOC);

        return $row;
        
    }

    function fetchAll( $response ){

        $aData = [];

        while( $row = $response->fetchArray(SQLITE3_ASSOC) ){
            $aData[] = $row;
        }

        return $aData;

    }

}