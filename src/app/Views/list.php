<main role="main">

    <section class="jumbotron text-center">
        <div class="container">
            <br>
            <h1 class="jumbotron-heading">Lista de livros</h1>
            <br>
            <form method="GET">
                <div class="input-group mb-3">
                    <input type="text" name="search" value="<?php echo $search; ?>" class="form-control" placeholder="Buscar livro" >
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit">Buscar</button>
                    </div>
                </div>
            </form>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">

                <?php if(count($books) == 0) {?>
                    <div class="alert alert-warning w-100" role="alert">
                        Nenhum livro encontrado.
                    </div>
                <?php } else { ?>

                    <?php foreach($books as $book) {?>

                        <div class="col-md-4">
                            <div class="card mb-4 shadow-sm">
                                <img class="card-img-top" src="<?php echo $book['thumbnail']?>">
                                <div class="card-body">
                                    <p class="card-text text-center"><b><?php echo $book['title']?></b></p>
                                    <p class="card-text"><?php echo $book['description']?></p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <button type="button" onclick="Index.onClickSetFavorite('<?php echo ($book['code'])?>')" class="btn btn-sm btn-outline-secondary">Marcar como favorito</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    <?php }?>

                <?php }?>
            </div>
        </div>
    </div>

    </main>