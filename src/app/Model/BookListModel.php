<?php

namespace TestePHP;

use TestePHP\Core\Model;
use TestePHP\Library\common;
use TestePHP\Library\Db;

class BookListModel extends Model {

    private $common;

    function __construct(Common $oCommon) {
        parent::__construct($oCommon);

        $this->common = $oCommon;
        $this->db = new Db;
    }

    public function getBooks( $search = '' ){

        if( $search == '' ){
            $search = 'Ensino';
        }

        $tuCurl = curl_init();
        curl_setopt($tuCurl, CURLOPT_URL, "https://www.googleapis.com/books/v1/volumes?projection=lite&q={$search}");
        curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);

        $tuData = curl_exec($tuCurl);
        $data = json_decode( $tuData, true );

        if( $data['totalItems'] == 0 ){
            return [];
        }

        $aBooks = [];

        for( $i = 0; $i < 6; $i++ ){

            $item = $data['items'][$i];

            $aBooks[] = [
                'code' => ( !empty($item['id']) ? $item['id'] : null ),
                'title' => ( !empty($item['volumeInfo']['title']) ? $item['volumeInfo']['title'] : '' ),
                'description' => ( !empty($item['volumeInfo']['description']) ? $item['volumeInfo']['description'] : '' ),
                'thumbnail' => ( !empty($item['volumeInfo']['imageLinks']['thumbnail']) ? $item['volumeInfo']['imageLinks']['thumbnail'] : '' )
            ];
            
        }

        return $aBooks;

    }

    public function getFavoriteBooks(){

        $favoritBooks = $this->getFavorite();

        $aBooks = [];

        foreach( $favoritBooks as $book ){

            $tuCurl = curl_init();
            curl_setopt($tuCurl, CURLOPT_URL, "https://www.googleapis.com/books/v1/volumes/{$book['code']}");
            curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);

            $tuData = curl_exec($tuCurl);
            $data = json_decode( $tuData, true );

            $aBooks[] = [
                'code' => ( !empty($data['id']) ? $data['id'] : null ),
                'title' => ( !empty($data['volumeInfo']['title']) ? $data['volumeInfo']['title'] : '' ),
                'description' => ( !empty($data['volumeInfo']['description']) ? $data['volumeInfo']['description'] : '' ),
                'thumbnail' => ( !empty($data['volumeInfo']['imageLinks']['thumbnail']) ? $data['volumeInfo']['imageLinks']['thumbnail'] : '' )
            ];

        }

        return $aBooks;

    }

    public function setFavorite( $code ){

        $hasInserted = $this->getFavorite($code);

        if( !empty($hasInserted) ){
            return [
                'success' => false,
                'message' => 'Já existe este livro nos favoritos'
            ];
        }

        $sql = " INSERT INTO favoritbooks(
                    code
                 ) VALUES(
                    '".$code."'
                 )";

        $response = $this->db->executeQuery($sql);

        if( !$response['success'] ){
            return [
                'success' => false,
                'message' => 'Ocorreu um erro ao gravar.'
            ];
        } else {
            return [
                'success' => true,
                'message' => 'Livro salvo como favorito.'
            ];
        }

    }

    public function deleteFavorite( $code ){

        $sql = " DELETE 
                   FROM favoritbooks 
                  WHERE code = '".$code."'";

        $response = $this->db->executeQuery($sql);

        if( !$response['success'] ){
            return [
                'success' => false,
                'message' => 'Ocorreu um erro ao deletar.'
            ];
        } else {
            return [
                'success' => true,
                'message' => 'Livro excluido dos favoritos.'
            ];
        }

    }

    public function getFavorite( $code = null ){

        $sFilter = "";

        if( $code != null ){
            $sFilter = " AND code == '".$code."'";
        }

        $sql = " SELECT code
                   FROM favoritbooks
                  WHERE 1=1
                  {$sFilter}";

        $response = $this->db->executeQuery($sql);

        if( !$response['success'] ){
            return false;
        }

        $data = $this->db->fetchAll($response['response']);

        return $data;

    }

}