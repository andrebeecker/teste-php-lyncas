<?php
namespace TestePHP;

use TestePHP\BookListModel;
use TestePHP\Core\Controller;
use TestePHP\Library\Common;

class BookListController extends Controller {

    private $model;

    /**
     * __construct
     *
     * @param  mixed $oCommon
     *
     * @return void
     */
    function __construct(Common $oCommon) {
        parent::__construct($oCommon);
        
        $this->model = new BookListModel($oCommon);    
    }

    public function setPost( $aPost ){
        $this->POST = $aPost;
    }

    /**
     * index
     *
     * @param mixed $request
     * @return Html
     */
    public function index( $request ){

        $search = ( isset($request['search']) ? $request['search'] : '' );

        $books = $this->model->getBooks( $search );

        $this->setVars([
            'search' => $search,
            'books' => $books,
        ]);

        $aJs = ['./assets/js/index.js'];
        $aCss = ['./assets/css/index.css'];

        $this->header( $request , $aCss, 'index');

        $this->render('list');

        $this->footer( $aJs );

    }

    /**
     * index
     *
     * @param mixed $request
     * @return Html
     */
    public function request( $request ){

        if( empty($request['action']) ){
            return false;
        }

        $response = false;

        switch($request['action']){
            case 'setFavorite':
                $response = $this->model->setFavorite( $request['code'] );
                break;
            case 'deleteFavorite':
                $response = $this->model->deleteFavorite( $request['code'] );
                break;
        }

        return $response;

    }

    /**
     * lista dos livros favoritos
     *
     * @param Array $request
     * @return Html
     */
    public function favorites( $request ){

        $books = $this->model->getFavoriteBooks();

        $this->setVars([
            'books' => $books,
        ]);

        $aJs = ['./assets/js/index.js'];
        $aCss = ['./assets/css/index.css'];

        $this->header( $request , $aCss, 'favorites');

        $this->render('favorites');

        $this->footer( $aJs );

    }

}