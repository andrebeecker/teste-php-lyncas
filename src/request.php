<?php

header('Content-Type: application/json');

require_once __DIR__ . '/../core/bootstrap.php';

ini_set('display_errors',1);

Use TestePHP\Library\Common;
Use TestePHP\BookListController;

$post = filter_input_array(INPUT_POST);
$get = filter_input_array(INPUT_GET);

$request = ( !empty($post) ? $post : [] ) + ( !empty($get) ? $get : [] );

$oCommon = new Common();
$list = new BookListController( $oCommon );

$response = $list->request( $request );

echo json_encode($response);