Index = {
    init: function() {

    },

    onClickSetFavorite( code ){
        $.ajax({
            method: "POST",
            url: "./request.php",
            data: { 
                action: "setFavorite", 
                code: code
            }
        })
        .done(function( msg ) {

            if( msg.success ){
                Swal.fire(
                    'Successo!',
                    'Livro colocado nos favoritos',
                    'success'
                )

            } else {
                Swal.fire(
                    'Erro!',
                    msg.message,
                    'error'
                )
            }

        });
    },

    onClickDeleteFavorite( code ){

        Swal.fire({
            title: 'Aviso',
            text: "Deseja realmente quer excluir este livro dos favoritos?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Sim, Excluir!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    method: "POST",
                    url: "./request.php",
                    data: { 
                        action: "deleteFavorite", 
                        code: code
                    }
                })
                .done(function( msg ) {

                    if( msg.success ){
                        Swal.fire(
                            'Successo!',
                            'Livro excluido dos favoritos',
                            'success'
                        );

                        window.location.reload()

                    } else {
                        Swal.fire(
                            'Erro!',
                            msg.message,
                            'error'
                        )
                    }

                });
            }
        })

        
    }
  
};

$(document).ready(function() {
  Index.init();
});
