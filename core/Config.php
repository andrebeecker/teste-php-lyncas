<?php

function getDomain(){
    $tmp = explode('.', $_SERVER['HTTP_HOST']);
    $result = '';
    if(sizeof($tmp) > 0){
        for($i=1; $i < sizeof($tmp); $i++){
            $result .= ($i > 1) ? '.' : '';
            $result .= $tmp[$i];
        }    
    }
    else{
        $result = $_SERVER['HTTP_HOST']; 
    }
    return $result;
}

require_once(__DIR__.'/../defines/Development.php');