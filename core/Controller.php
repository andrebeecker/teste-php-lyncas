<?php

namespace TestePHP\Core;
use TestePHP\Library\Common;
use TestePHP\Core\View;

class Controller extends View{
    /** @var string Diretório onde será buscado o arquivo da view */
    private $view_dir;

    /** @var array Array de variaveis que serão enviadas para a view */
    private $vars = [];
    private $js = [];
    private $css = [];

    public $common;

    function __construct(Common $common){
        $this->common = $common;

        $this->setViewDir();
    }

    /**
     * Faz o include do arquivo de view
     */
    public function render($view){
        try {

            if(file_exists($this->view_dir.$view.'.php')) {   
                extract($this->vars);
                include $this->view_dir.$view.'.php';
            } else {
                throw new \Exception("Arquivo de view '$view' não existe");
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Seta um array de variáveis que será enviado para a view
     */
    public function setVars(Array $vars = null){
        if(!$vars) {
            return;
        }

        $this->vars = $this->vars + $vars;
    }

    /**
     * Permite setar o diretório específico de views para o controller
     */
    public function setViewDir($viewDir = null){
        $reflection = new \ReflectionClass(get_called_class());
        if(isset($viewDir)) {
            $this->view_dir = dirname($reflection->getFileName()) . DS .'..' . DS . 'Views' . DS . $viewDir . DS;
        } else {
            $this->view_dir = dirname($reflection->getFileName()) . DS .'..' . DS . 'Views' . DS;
        }
    }

    public function getViewDir(){
        return $this->view_dir;
    }

}