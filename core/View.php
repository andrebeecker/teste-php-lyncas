<?php
namespace TestePHP\Core;

use TestePHP\Library\Common;

class View
{
    public $Common;

    function __construct(Common $common){
        $this->Common = $common;
    }

    public function header( $request, $aCss = [], $linkActive = null ) {

        $html = '
        <!DOCTYPE html>
        <html lang="pt-br">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            
            <link rel="stylesheet" type="text/css" href="./node_modules/tooltipster/dist/css/tooltipster.bundle.min.css">
            <link rel="stylesheet" type="text/css" href="./node_modules/bootstrap/dist/css/bootstrap.min.css">
            <link rel="stylesheet" type="text/css" href="./node_modules/sweetalert2/dist/sweetalert2.min.css">
            <link rel="stylesheet" type="text/css" href="./node_modules/@fortawesome/fontawesome-free/css/all.min.css">

      ';

        foreach( $aCss as $css ){
            $html.= '<link rel="stylesheet" type="text/css" href="'.$css.'">';
        }

        $html.= '<link rel="icon" type="image/png" sizes="32x32" href="/inscricao/assets/img/favicons/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="16x16" href="/inscricao/assets/img/favicons/favicon-16x16.png">

            <title>Livros</title>

            <meta name="theme-color" content="#ffdb1b">

        </head>
        <body>
            <header>
                <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                    <a class="navbar-brand" href="#">Livros</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item '.( $linkActive == 'index' ? 'active' : '' ).'">
                            <a class="nav-link" href="/index.php">Pesquisa <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item '.( $linkActive == 'favorites' ? 'active' : '' ).'">
                            <a class="nav-link" href="/favorites.php">Meus favoritos</a>
                        </li>
                    </ul>
                    </div>
                </nav>
            </header>';

        echo $html;
        
    }

    public function footer( $aJs = []) {
        $html = '
        
            <footer class="text-muted">
                
            </footer>

            <script src="./node_modules/jquery/dist/jquery.min.js"></script>
            <script src="./node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
            <script src="./node_modules/sweetalert2/dist/sweetalert2.all.min.js"></script>'; 

        foreach( $aJs as $js ){
            $html.= '<script src="'.$js.'"></script>';
        }

        $html.= '
        </body>
        </html>';

        echo $html;

    }
    
}