<?php

define('DS', DIRECTORY_SEPARATOR);
define('ROOT_LIBRARY', dirname(__FILE__) . DS . '..');
define('ROOT', dirname(__FILE__) . DS . '..'. DS .'src'. DS .'app');
define('PROJECT_NAMESPACE', 'TestePHP\\');

// CLASSES DO CORE
require_once __DIR__ . '/Config.php';
require_once __DIR__ . '/Autoload.php';
require_once __DIR__ . '/View.php';
require_once __DIR__ . '/Controller.php';
require_once __DIR__ . '/Model.php';

$autoload = new TestePHP\Autoload;

spl_autoload_register( [ $autoload, 'loadControllers' ] );
spl_autoload_register( [ $autoload, 'loadModels' ] );
spl_autoload_register( [ $autoload, 'loadHelpers' ] );
spl_autoload_register( [ $autoload, 'loadLibraries' ] );