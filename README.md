# Teste para PHP Lyncas

## Tecnologias usadas

- PHP
- Javascript
- CSS
- SQLite3

## Desenvolvimento

Para desenvolver a aplicação usei o PHP puro sem framework com um MVC feito na mão, Javascript, CSS com Bootstrap, base de dados SQLite3 para armazenar os favoritos.
Usei SQLite por ser mais fácil de rodar o código em qualquer lugar, facilitando a análise.
Desenvolvi em aproximadamente 4 horas.
Trabalho bastante com PHP então essa é uma facilidade, não tive nenhuma dificuldade.

## Instruções para rodar

- Não é necessário instalar nenhuma dependência
- Servidor apache ou nginx
- Apontar root para teste-php-lyncas/src

Para rodar o projeto basta fazer o clone, rodar em um servidor apache ou nginx e apontar o vhost para /src.

## Funcionalidades

- Listar livros, com opção de pesquisa.
- Salvar livros em favoritos (Salvo em SQLite).
- Listar livros favoritos.
- Excluir livro dos favoritos (com confirmação).

### Desde já agradeço a oportunidade.